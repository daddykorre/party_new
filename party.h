#ifndef PARTY_H
#define PARTY_H
#include <string>
#include <string.h>
#include <iostream>

using namespace std;

class party
{
    public:
        party();
        int xp, lvl;
        string name;
        //Asks the user to define the characters names
        void read_names();
        //Asks the user to define the characters XP and level
        void read_xp_lvl();
        //Prints all character names with XP and level
        void print();
        //Sorts characters alphabetically
        void alpha_sort();

    protected:

    private:
};

class character: public party{
    public:
        CHARACTER();

    private:


};

#endif // PARTY_H
